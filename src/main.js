import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import Argon from "./plugins/argon-kit"
import './registerServiceWorker'
import jQuery from 'jquery'
import VueCarousel from '@chenfengyuan/vue-carousel';

Vue.component('map-view', require('./views/MapView.vue').default)
Vue.component('track-view', require('./views/TrackView.vue').default)
Vue.component('winner-view', require('./views/WinnerView.vue').default)


global.$ = global.jQuery = jQuery
require('jvectormap');
require('./plugins/jquery-jvectormap-world-mill-en.js');
Vue.use(VueCarousel);

Vue.config.productionTip = false
Vue.use(Argon)
new Vue({
  router,
  render: h => h(App)
}).$mount("#app")
