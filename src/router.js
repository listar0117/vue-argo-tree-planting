import Vue from "vue"
import Router from "vue-router"
import AppHeader from "./layout/AppHeader"
import Register from "./views/Register.vue"
import HomePage from './views/Landing.vue'
import MapView from './views/MapView.vue'
import TrackView from './views/TrackView.vue'
import WinnerView from './views/WinnerView.vue'
import TreePlanting from './views/TreePlanting.vue'
Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        header: AppHeader,
        default: HomePage,
      }
    },
    {
      path: '/tree-planting',
      name: 'tree-planting',
      components: {
        header: AppHeader,
        default: TreePlanting
      }
    },
    {
      path: "/register",
      name: "register",
      components: {
        header: AppHeader,
        default: Register,
      }
    },
    {
      path: '/map-view',
      name: 'map-view',
      components: {
        header: AppHeader,
        default: MapView
      }
    },
    {
      path: '/track-view',
      name: 'track-view',
      components: {
        header: AppHeader,
        default: TrackView
      }
    },
    {
      path: '/winner-view',
      name: 'winner-view',
      components: {
        header: AppHeader,
        default: WinnerView
      }
    },
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
